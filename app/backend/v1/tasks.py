import logging
import docker
import os
import requests
import json
import tempfile
import threading
import urlparse
import urllib
import etcd
import subprocess as sp
from celery import shared_task, Task
import base64
import uuid
from pwd import getpwnam
from datetime import datetime

__UPLOADS__ = "uploads/"
BASE_IMAGE = 'xarthisius/hublaunch'
BASE_IMAGE_TAG = 'latest'
CONTAINER_STORAGE = "/tmp/containers.json"
CONTAINER_VERSION = '1.0.0'
SCHEME = "irods"
NETLOC = ""
TIME_CAP = 2  # in hours
DOCKER_API_VER = '1.19'

IRODS_DATADIR = os.environ.get("IRODS_DATADIR", "/NDS/home/kacperk/data/")
HOST_IP = os.environ.get('COREOS_PRIVATE_IPV4', "127.0.0.1")
FIDOPASS = os.environ.get('ytfidopassword', '3nthr0py')
TOKEN = os.environ.get('CONFIGPROXY_AUTH_TOKEN', 'verysecrettoken')
MOUNTS_DIR = '/tmp/mounts/'
name = "xarthisius"
workdir = "/home/user/work/"
lock = threading.Lock()

ALLOWED_IMAGES = dict([
    ("xarthisius/hublaunch", 8888),
    ("ndslabs/rstudio", 80),
    ("ndslabs/devenv", 8888),
])

# workaround for supervisor issue FIXME
os.environ['USER'] = "user"
os.environ['HOME'] = "/home/user"

RUNMODE = "django"
BASE_DOCKER_URL = "tcp://141.142.234.27:2375"

try:
    from backend import celeryconfig
    from celery import Celery
    celery = Celery()
    celery.config_from_object(celeryconfig)
    RUNMODE = "tcelery"
except ImportError:
    logging.debug("Running in Django, not re-loading celery.")


class ContainerException(Exception):

    """
    There was some problem generating or launching a docker container
    for the user
    """
    pass


def get_image(image_name, image_tag):
    dcli = docker.Client(base_url=BASE_DOCKER_URL, timeout=10,
                         version=DOCKER_API_VER)
    repo_tag = image_name + ":" + image_tag
    # TODO catch ConnectionError - requests.exceptions.ConnectionError
    for image in dcli.images():
        if repo_tag in image['RepoTags']:
            return image
    raise ContainerException("No image found")
    return None


def remember_container(payload):
    client = etcd.Client(host=HOST_IP, port=4001)
    client.write("/containers/%s" % payload["id"], json.dumps(payload))


def forget_container(cont_id):
    client = etcd.Client(host=HOST_IP, port=4001)
    info = json.loads(client.read('/containers/%s' % cont_id).value)
    if "sage" in info["proxy_prefix"]:
        proxy_entry = '/sage/proxies/%s' % info["proxy_prefix"].split('/')[-1]
    else:
        proxy_entry = '/proxy/proxies/%s' % info["proxy_prefix"].rstrip('/')

    try:
        client.delete(proxy_entry)
    except etcd.EtcdKeyNotFound:
        logging.info("Tried to remove non-existing '%s'" % proxy_entry)
    client.delete('/containers/%s' % cont_id)


def docker_url(ip):
    return "tcp://%s:2375" % ip


def get_container_meta(cont_id):
    cid = cont_id.replace('"', '').replace("'", '')
    client = etcd.Client(host=HOST_IP, port=4001)
    try:
        json.loads(client.get("/containers/%s" % cid).value)
    except KeyError:
        raise KeyError("Container %s is not registered in etcd" % cid)

    dcli = docker.Client(base_url=BASE_DOCKER_URL,
                         version=DOCKER_API_VER,
                         timeout=120)
    return cid, dcli


class WorkerSpecificTask(Task):
    abstract = True

    @property
    def queue(self):
        if RUNMODE == 'tcelery':
            return ''
        try:
            import celery.app.control
            app = celery.app.App()
            i = app.control.inspect()
            q = i.active_queues()

            for worker in q:
                for queue in q[worker]:
                    if queue['name'] == 'other_tasks':
                        return queue['routing_key']
        except:
            return 'create_tasks'


@shared_task(base=WorkerSpecificTask)
def create_job_docker(*args, **kwargs):
    mountpoint = '/tmp'
    tmpdir = None
    binds = {}
    u = uuid.uuid4().hex
    queue = create_job_docker.queue

    client = etcd.Client(host=HOST_IP, port=4001)
    dcli = docker.Client(base_url=BASE_DOCKER_URL,
                         version=DOCKER_API_VER,
                         timeout=10)

    env = {'ytfidopassword': FIDOPASS,
           'COREOS_PRIVATE_IPV4': HOST_IP,
           'irodsHost': client.read('/irods/host').value,
           'irodsPort': client.read('/irods/port').value,
           'irodsZone': client.read('/irods/zone').value}

    image_name = kwargs["image_name"]
    if image_name not in ALLOWED_IMAGES:
        return json.dumps({"error": "Image %s in not allowed" % image_name})

    image_tag = kwargs["image_tag"]
    if image_name in ("xarthisius/hublaunch", "ndslabs/devenv"):
        cmd = ["/usr/local/bin/setup_irodsuser"]
        proxy_prefix = "/py/%s" % u
    elif image_name == "ndslabs/rstudio":
        cmd = ["/usr/bin/supervisord"]
        proxy_prefix = "/R/%s/" % u
    else:
        cmd = ["exit"]
        proxy_prefix = "/null"
    env['HOST_PREFIX'] = proxy_prefix

    logging.info('kwargs: %s' % kwargs)
    if 'script' in kwargs:
        file = kwargs['script']
        with tempfile.NamedTemporaryFile(suffix='.py', mode='w',
                                         delete=False) as auxf:
            if(isinstance(file, basestring)):
                auxf.write(file)

        filename = kwargs['scriptname']
        cmd.append(auxf.name)
    else:
        filename = None

    if "data" in kwargs:
        env.update({'mounts': json.dumps(kwargs["data"])})

    env['WEBDAV_HOST'] = 'http://use.yt/owncloud/remote.php/webdav'
    env['WEBDAV_USER'] = 'ytfido'
    env['WEBDAV_OTP'] = FIDOPASS

    inner_port = ALLOWED_IMAGES[image_name]
    binds.update({"/tmp": {"bind": mountpoint}})

    image = get_image(image_name, image_tag)
    cont = dcli.create_container(
        image['Id'], cmd,
        hostname="{user}box".format(user=name.split('-')[0]),
        ports=[inner_port],
        environment=env,
        working_dir=workdir,
        volumes=["/tmp"],
    )
    # check_memory()  # TODO reimplement me
    # dcli.start(cont['Id'], binds=binds, publish_all_ports=True)  # OLD
    # TODO figure out minimum set of reqs for fuse to work instead of using
    # priviledged=True
    dcli.start(cont['Id'], privileged=True, volumes_from="celery",
               publish_all_ports=True)
    if filename is None:
        outward_port = dcli.port(cont['Id'], inner_port).pop()["HostPort"]
        client.set('/proxy/proxies%s' % proxy_prefix,
                   'http://%s:%s%s' % (HOST_IP, outward_port, proxy_prefix))

    info = dcli.inspect_container(cont['Id'])
    remember_container(
        {"name": name, "id": cont.get('Id'), "tmpdir": tmpdir,
         "image_name": image_name, "image_tag": image_tag,
         "notebook": kwargs["notebook"], "host": HOST_IP,
         "proxy_prefix": proxy_prefix,
         "StartedAt": info["State"]["StartedAt"]}
    )

    return json.dumps(
        {'Id': cont['Id'],
         'script': filename,
         'queue': queue,
         'proxy_prefix': proxy_prefix,
         'uuid': u})


@shared_task(base=WorkerSpecificTask)
def get_job_state_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"State": "UNKNOWN",
                           "error": "Container doesn't exist"})

    try:
        info = dcli.inspect_container(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"State": "UNKNOWN", "error": e.explanation})
    state = info['State']
    # {u'Pid': 0, u'Paused': False, u'Running': False,
    #  u'FinishedAt': u'2014-10-13T18:13:18.199756378Z',
    #  u'Restarting': False, u'StartedAt': u'2014-10-13T17:13:15.703794407Z',
    #  u'ExitCode': 0}
    if state["Running"]:
        return json.dumps({"State": "RUNNING",
                           "StartedAt": state["StartedAt"]})
    if state['ExitCode'] != 0:
        return json.dumps({"State": "FAILED", "ExitCode": state['ExitCode']})
    return json.dumps({"State": "COMPLETED"})


@shared_task(base=WorkerSpecificTask)
def get_job_stdout_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"State": "UNKNOWN",
                           "error": "get_container_meta failed"})

    try:
        logs = dcli.logs(cont_id, stdout=True, stderr=True,
                         stream=False, timestamps=False)
    except docker.errors.APIError as e:
        return json.dumps({"stdout": None, "error": e.explanation})
    return json.dumps({"stdout": logs})


@shared_task(base=WorkerSpecificTask)
def get_job_metric_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"State": "UNKNOWN", "error": e.message})

    try:
        data = dcli.inspect_container(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps(data)


@shared_task(base=WorkerSpecificTask)
def get_container_info(*args, **kwargs):
    client = etcd.Client(host=HOST_IP, port=4001)
    if "cont_id" in kwargs["_query"]:
        cont_id = kwargs['_query']['cont_id'][0]
        cid = cont_id.replace('"', '').replace("'", '')
        try:
            payload = client.get(os.path.join("/containers", cid)).value
        except etcd.EtcdException:
            return json.dumps({"error": "No such container"})
    else:
        # Get info about containers from etcd
        cont_info = []
        for child in client.read('/containers').children:
            if child.dir:
                continue
            foo = {}
            cdict = json.loads(child.value)
            for key in ["id", "image_name", "image_tag", "StartedAt"]:
                try:
                    foo[key] = cdict[key]
                except KeyError:
                    pass
            cont_info.append(foo)

        payload = json.dumps(cont_info)

    return payload


@shared_task(base=WorkerSpecificTask)
def get_job_changes_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"State": "UNKNOWN", "error": e.message})

    try:
        data = dcli.diff(cont_id)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps(data)


@shared_task(base=WorkerSpecificTask)
def get_job_filepath_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"State": "UNKNOWN", "error": e.message})

    path = kwargs['_query']['path'][0]
    try:
        raw = dcli.copy(cont_id, path)
    except docker.errors.APIError as e:
        return json.dumps({"error": e.explanation})
    return json.dumps({'file': base64.b64encode(raw.read())})


@shared_task(base=WorkerSpecificTask)
def get_docker_image_list(*args, **kwargs):
    # Ultimately, we will want a store somewhere that lists the names of docker
    # images that are "sanctioned" for use.  For now, this will be a hardcoded
    # list.
    return json.dumps(list(ALLOWED_IMAGES.keys()))


@shared_task
def get_list_data_irods(*args, **kwargs):
    init_irods()
    idirs = []
    for idir in sp.check_output('ils %s' % IRODS_DATADIR,
                                shell=True).split('\n'):
        if idir.startswith('  C- '):
            idirs.append(os.path.basename(idir.split()[-1]))
    return json.dumps([
        {"name": d, "url": "%s://%s%s" %
         (SCHEME, NETLOC, os.path.join(IRODS_DATADIR, d))} for d in idirs
    ])


@shared_task
def list_containers_docker(*args, **kwargs):
    client = etcd.Client(host=HOST_IP, port=4001)
    etc = dict((child.key, child.value)
               for child in client.read('/containers').children)
    containers = [os.path.relpath(k, "/containers") for k in etc.keys()]
    return json.dumps(
        {"containers": containers}
    )


@shared_task
def stop_container_docker(*args, **kwargs):
    try:
        cont_id, dcli = get_container_meta(kwargs['_query']['cont_id'][0])
    except KeyError as e:
        return json.dumps({"state": "UNKNOWN", "error": e.message})

    try:
        data = dcli.inspect_container(cont_id)
    except docker.errors.APIError as e:
        if e.is_client_error():  # container most like doesn't exist
            forget_container(cont_id)
            return json.dumps(
                {"state": "success", "msg": "%s removed from etcd" % cont_id}
            )
        else:
            return json.dumps({"state": "UNKNOWN", "error": e.explanation})

    if data["State"]["Running"]:
        try:
            dcli.stop(cont_id, 5)
            payload = {
                "state": "success", "msg": "container %s stopped" % cont_id
            }
        except docker.errors.APIError as e:
            return json.dumps({"state": "failed", "error": e.explanation})
    else:
        payload = {"state": "failed", "error": "%s is not running" % cont_id}

    forget_container(cont_id)
    return json.dumps(payload)


@shared_task
def cull_containers(*args, **kwargs):
    ncount = 0
    dummy_kwargs = {"_query": {}}
    for container in json.loads(get_container_info(**dummy_kwargs)):
        try:
            lactive = datetime.strptime(container["StartedAt"][:19],
                                        "%Y-%m-%dT%H:%M:%S")
        except ValueError:
            lactive = datetime.min
        last_active_h = (datetime.utcnow() - lactive).total_seconds() // 3600
        if last_active_h > TIME_CAP:
            dummy_kwargs = {"_query": {"cont_id": [container["id"]]}}
            result = stop_container_docker(**dummy_kwargs)
            logging.info("%s\n" % result)
            ncount += 1
    logging.info("Culled %i containers" % ncount)


@shared_task
def start_sage(*args, **kwargs):
    proxy_hash = uuid.uuid4().hex[:6]

    client = etcd.Client(host=HOST_IP, port=4001)
    dcli = docker.Client(base_url=BASE_DOCKER_URL,
                         version=DOCKER_API_VER,
                         timeout=10)

    env = {
        'COREOS_PRIVATE_IPV4': HOST_IP,
        'SAGE2_HOST_URL_PREFIX': "%s/" % proxy_hash
    }

    image_name = "hub.yt/sage/sage2"
    image_tag = "latest"
    image = get_image(image_name, image_tag)
    cont = dcli.create_container(
        image['Id'],
        ports=[443, 80],
        environment=env,
    )
    dcli.start(cont['Id'], privileged=False,
               volumes_from=["sage2Config", "sage2Keys"],
               publish_all_ports=True)

    outward_port = dcli.port(cont['Id'], 443).pop()["HostPort"]
    client.set('/sage/proxies/%s' % proxy_hash,
               'http://%s:%s' % (HOST_IP, outward_port))

    info = dcli.inspect_container(cont['Id'])
    remember_container(
        {"name": name, "id": cont.get('Id'), "tmpdir": None,
         "image_name": image_name, "image_tag": image_tag,
         "notebook": False, "host": HOST_IP,
         "proxy_prefix": "/sage/%s" % proxy_hash,
         "StartedAt": info["State"]["StartedAt"]}
    )

    payload = {'url': 'https://hub.yt/sage/%s/' % proxy_hash}
    return json.dumps(payload)


@shared_task
def umount_tmpdir_fuse(*args, **kwargs):
    return   # I think this is obsolete now
    dcli = docker.Client(base_url=BASE_DOCKER_URL,
                         version=DOCKER_API_VER,
                         timeout=10)

    containers = []
    client = etcd.Client(host=HOST_IP, port=4001)
    for container in client.get("/containers").children:
        if container["host"] == HOST_IP:
            containers.append({"id": os.path.basename(container.key),
                               "mount": container.value["mount"]})

    cwd = os.getcwd()
    os.chdir(MOUNTS_DIR)
    for container in containers:
        try:
            info = dcli.inspect_container(container["id"])
            state = info["State"]
            if container["mount"] is None:
                continue
            if not state["Running"] and not state["Paused"] \
                    and os.path.ismount(container["mount"]):
                status = sp.call("fusermount -u " + container["mount"],
                                 shell=True)
                if status == 0:
                    os.removedirs(
                        os.path.relpath(container["mount"], MOUNTS_DIR)
                    )
        except docker.errors.APIError as e:
            logging.debug("inspecting gave %s" % e.explanation)
            pass
    os.chdir(cwd)


def prepare_dir_irods(data):
    init_irods()
    if not os.path.isdir(MOUNTS_DIR):
        os.mkdir(MOUNTS_DIR)
    tmpdir = tempfile.mkdtemp(prefix=MOUNTS_DIR)
    path = urlparse.urlparse(data).path
    dname = os.path.split(path)[-1]   # not safe, fixme
    os.mkdir(os.path.join(tmpdir, dname))
    cmd = 'icd ' + path + ' && ' + \
        'irodsFs -o allow_other ' + os.path.join(tmpdir, dname)
    sp.call(cmd, shell=True)
    return tmpdir


@shared_task(base=WorkerSpecificTask)
def register_file_irods(*args, **kwargs):
    init_irods()
    url = kwargs["resource"]["url"]
    fname = os.path.basename(urlparse.urlparse(url).path)
    tmpdir = tempfile.mkdtemp()
    tmpfile = os.path.join(tmpdir, fname)
    irods_dir = os.path.join(IRODS_DATADIR, os.path.splitext(fname)[0])
    kwargs["resource"].update({"local": "epiphyte://%s" % irods_dir})

    try:
        sp.check_call('ils ' + irods_dir, shell=True)
        kwargs["resource"].update({"status": "exists"})
        return json.dumps(kwargs["resource"])
    except sp.CalledProcessError:
        pass

    urllib.urlretrieve(url, tmpfile)
    sp.call('imkdir ' + irods_dir, shell=True)  # create dir
    sp.call('iput -R defaultResc ' + tmpfile + " " + irods_dir, shell=True)
    kwargs["resource"].update({"status": "new"})

    return json.dumps(kwargs["resource"])


def init_irods():
    # IRODS server may change anytime. It's better to always do this
    ihome = os.path.join(getpwnam("user").pw_dir, ".irods")
    client = etcd.Client(host=os.environ.get('COREOS_PRIVATE_IPV4', None),
                         port=4001)
    with open(os.path.join(ihome, ".irodsEnv"), 'w') as fh:
        fh.write("irodsHost %s\n" % client.read('/irods/host').value)
        fh.write("irodsPort %s\n" % client.read('/irods/port').value)
        fh.write("irodsZone %s\n" % client.read('/irods/zone').value)
        fh.write("irodsUserName ytfido\n")
    cmd = "iinit %s" % os.environ.get('ytfidopassword', '3nthr0py')
    sp.call(cmd, shell=True)
