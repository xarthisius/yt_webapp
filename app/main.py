import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import logging
import tcelery


from tcelery_req_handler import routes, tcelery_async_routes

import handler
import config
import backend.v1

tcelery.setup_nonblocking_producer()

api_version = "v1"


class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            tcelery_async_routes(
                r"/%s/status/?" % api_version,
                get_tasks=[backend.v1.get_job_state_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/register/?" % api_version,
                post_tasks=[backend.v1.register_file_irods],
                handler=handler.FileRegisterHandler),
            tcelery_async_routes(
                r"/%s/stdout/?" % api_version,
                get_tasks=[backend.v1.get_job_stdout_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/info/?" % api_version,
                get_tasks=[backend.v1.get_container_info],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/metric/?" % api_version,
                get_tasks=[backend.v1.get_job_metric_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/diff/?" % api_version,
                get_tasks=[backend.v1.get_job_changes_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/start_sage/?" % api_version,
                get_tasks=[backend.v1.start_sage],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/upload/?" % api_version,
                post_tasks=[backend.v1.create_job_docker],
                handler=handler.FileUploadHandler),
            tcelery_async_routes(
                r"/%s/list_data/?" % api_version,
                get_tasks=[backend.v1.get_list_data_irods],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/list_containers/?" % api_version,
                get_tasks=[backend.v1.list_containers_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/stop/?" % api_version,
                get_tasks=[backend.v1.stop_container_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/download/?" % api_version,
                get_tasks=[backend.v1.get_job_filepath_docker],
                handler=handler.AppHandler),
            tcelery_async_routes(
                r"/%s/list_images/?" % api_version,
                get_tasks=[backend.v1.get_docker_image_list],
                handler=handler.AppHandler),
            (r"/", handler.MainHandler),
        ]
        settings = dict(
            title=u"Tornado Celery Handler",
            # template_path=os.path.join(os.path.dirname(__file__),
            #                            "templates"),
            # static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=False,
            cookie_secret=config.default.cookie_secret,
            debug=config.default.debug,
            autoescape=None,
        )
        if settings['debug']:
            logging.getLogger().setLevel(logging.DEBUG)
        else:
            logging.getLogger().setLevel(logging.INFO)
            logging.getLogger().setLevel(logging.WARNING)

        tornado.web.Application.__init__(self, routes(handlers), **settings)


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(config.default.server_port,
                       config.default.server_ipaddr)
    print("Server running in port %s:%d" %
          (config.default.server_ipaddr, config.default.server_port))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
