import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.escape
import json
import os

from tornado import gen
from tornado.web import asynchronous


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('main')


class FileUploadHandler(tornado.web.RequestHandler):
    @asynchronous
    @gen.coroutine
    def post(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.post_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments

        if "file" in self.request.files:
            script = self.request.files["file"].pop()
            kwargs['script'] = script["body"]
            kwargs['scriptname'] = script["filename"]
            kwargs["notebook"] = \
                os.path.splitext(kwargs['scriptname'])[1] == '.ipynb'
        else:
            kwargs["notebook"] = True

        form = self.request.arguments
        for key in ["image_name", "image_tag"]:
            if key in form:
                kwargs[key] = form[key].pop()
        kwargs['data'] = self.get_arguments("data")
        response = \
            yield self.task_handler.post_one(0, self.request.arguments,
                                             **kwargs)
        if response.result:
            self.write(response.result)
        else:
            self.raise404()
        self.finish()


class FileRegisterHandler(tornado.web.RequestHandler):
    @asynchronous
    @gen.coroutine
    def post(self, *args):
        if "Content-type" not in self.request.headers:
            self.write("Content-type not found in the request's header")
        if self.request.headers["Content-type"] != "application/json":
            self.write("Content is not json")
        else:
            kwargs = json.loads(self.request.body)
            response = \
                yield self.task_handler.post_one(0, self.request.arguments,
                                                 **kwargs)
            if response.result:
                self.write(response.result)
            else:
                self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def get(self, *args):
        self.raise404()
        self.finish()


class AppHandler(tornado.web.RequestHandler):
    @asynchronous
    @gen.coroutine
    def get(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.get_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.get_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result)
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def post(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.post_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        kwargs['files'] = self.request.files
        kwargs['form'] = self.request.arguments
        response = yield self.task_handler.post_one(0,
                                                    self.request.arguments,
                                                    **kwargs)
        if response.result:
            self.write(response.result)
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def put(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.put_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.put_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result)
        else:
            self.raise404()
        self.finish()

    @asynchronous
    @gen.coroutine
    def delete(self, *args):
        self.set_header("Content-Type", "application/json")
        kwargs = {}
        if not self.delete_tasks:
            self.raise404()
        kwargs['_query'] = self.request.query_arguments
        kwargs['_body'] = self.request.body_arguments
        response = yield self.task_handler.delete_one(0, *args, **kwargs)
        if response.result:
            self.write(response.result)
        else:
            self.raise404()
        self.finish()
