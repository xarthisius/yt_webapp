yt Hub Web interface
====================

      python main.py
      celery worker -A backend.v1 -Q payment_default1 --concurrency 2 --loglevel=INFO 
      celery beat -A backend.v1  # cron jobs

      curl -F "image_name=ndslabs/hublaunch" \
           -F "image_tag=latest" \
           -F "file=@script.py" \
           -F "data=irods:///NDS/home/kacperk/data/IsolatedGalaxy" \
           "http://localhost:8888/v1/upload"
      curl "http://localhost:8888/v1/stdout?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/diff?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/metric?cont_id=997231c3ce6"
      curl "http://localhost:8888/v1/download?cont_id=997231c3ce6&path=/results"
      curl -H "Content-type: application/json" -X POST \
           -d '{"resource": {"url": "http://www.google.com"}}' \
           http://localhost:8888/v1/register
