FROM ubuntu:trusty
MAINTAINER  Kacper Kowalik "xarthius.kk@gmail.com"

RUN apt-get update -qq && \
  apt-get install -qy curl sudo supervisor python-dev python-pip git-core libffi-dev libssl-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# add a user
RUN useradd -D --shell=/bin/bash && \
  useradd -m user && \
  echo "user:secret" | chpasswd && \
  adduser user sudo && \
  sudo -u user mkdir /home/user/yt_serve

# supervisor process manager
ADD supervisor/supervisord.conf /etc/supervisor/supervisord.conf

RUN git clone https://bitbucket.org/Xarthisius/yt_webapp.git yt_serve && \
  pip install -r yt_serve/app/requirements.txt && \
  pip install -r yt_serve/app/backend/v1/requirements.txt

ADD ./supervisor/yt_serve.conf /etc/supervisor/conf.d/yt_serve.conf
ADD ./supervisor/yt_worker.conf /etc/supervisor/conf.d/yt_worker.conf
ADD app/ /home/user/yt_serve/
EXPOSE 8888

CMD ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]
